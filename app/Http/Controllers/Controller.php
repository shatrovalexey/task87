<?php

namespace App\Http\Controllers;

require_once __DIR__ . '/../../..' . '/vendor/autoload.php';

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Feedback ;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index( ) {
		return view( 'feedback/create' ) ;
	}

	public function send( Request $request ) {
		$validator = Validator::make( $request->post( ) , [
			'fio' => 'required' ,
			'phone' => 'required|regex:/^\d{11,16}$/u' ,
			'message' => 'required' ,
		] ) ;

		if ( $validator->fails( ) ) {
			return redirect( )->back( )->withErrors( $validator ) ;
		}

		$model = new \App\Feedback\Feedback( ) ;
		$model->fio = $request->post( 'fio' ) ;
		$model->phone = $request->post( 'phone' ) ;
		$model->message = $request->post( 'message' ) ;
		$model->save( ) ;

		return redirect( )->back( )->with( 'msg' , $model->id ) ;
	}
}