<?php

namespace App\Feedback ;

use Illuminate\Database\Eloquent\Model ;

class Feedback extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title' , 'phone' , 'message' ,
    ] ;

	public $rules = [
		'fio' => 'required' ,
		'phone' => 'required|regex:/^\d{11,16}$/u' ,
		'message' => 'required' ,
	] ;

	/**
	* Загрузить драйвера
	*
	* @param string $class_path - путь к драйверам
	*
	* @return mixed - список загруженных драйверов
	*/
	protected function load_drivers( $class_path = '../app/Feedback' ) {
		$result = [ ] ;

		while ( $dh = opendir( $class_path ) ) {
			while ( $file_path = readdir( $dh ) ) {
				$file_path = implode( DIRECTORY_SEPARATOR , [ $class_path , $file_path , ] ) ;

				if ( ! is_file( $file_path ) ) {
					continue ;
				}

				$result[ $file_path ] = @include_once( $file_path ) ;
			}
			closedir( $dh ) ;

			if ( empty( $file_path ) ) {
				break ;
			}
		}

		return $result ;
	}

	/**
	* Получить произвольный драйвер
	*
	* @param string $class_path - путь к драйверам
	*
	* @return mixed - список загруженных драйверов
	*/
	protected function driver( ) {
		$this->load_drivers( ) ;

		while ( true ) {
			foreach ( get_declared_classes( ) as $class_name ) {
				if ( $class_name == __CLASS__ ) {
					continue ;
				}
				if ( ! preg_match( '{^\Q' . __NAMESPACE__ . '\E\\\(.+)}us' , $class_name , $matches ) ) {
					continue ;
				}
				if ( rand( 1 , 2 ) > 1 ) {
					continue ;
				}

				return new $class_name( ) ;
			}
		}
	}

	/**
	* Сохранить данные объекта
	*
	* @param mixed $options - настройки
	*
	* @return mixed - путь к записи и результат сохранения
	*/
	public function save( array $options = [ ] ) {
		$driver = $this->driver( ) ;

		return [
			'driver' => $driver ,
			'result' => $driver->save( $options )
		] ;
	}
}