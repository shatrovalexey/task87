<?php
use Illuminate\Http\File ;
use App\Feedback\Feedback ;

namespace App\Feedback ;

/**
* Хранение в файлах
*/
class FileStorage extends Feedback {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title' , 'phone' , 'message' ,
    ] ;

	public $rules = [
		'fio' => 'required' ,
		'phone' => 'required|phone' ,
		'message' => 'required' ,
	] ;

	/**
	* Сохранить данные объекта
	*
	* @param mixed $options - настройки
	*
	* @return string - путь к файлу
	* @throws \Exception
	*/
	public function save( array $options = [ ] ) {
		$file_dir = sys_get_temp_dir( ) ;
		$file_name = date( 'Y-m-d-H-i-s' ) ;
		$file_path = tempnam( $file_dir , $file_dir ) ;

		if ( ! file_put_contents( $file_path , $this->toJson( ) ) ) {
			throw new \Exception( 'Can not store into "' . $file_path . '"' ) ;
		}

		return $file_path ;
	}
}