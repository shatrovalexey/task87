<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>task-87</title>

		<style>
.form {
	padding: 20px ;
	margin: 20px ;
}
.form label {
	margin: 5px ;
}
.form label input ,
.form label textarea {
	width: 100% ;
}
.form label input[type="submit"] {
	width: auto ;
}
		</style>
    </head>
    <body>
		@if ( session( 'msg' ) )
		<div>{{session( 'msg' )}}</div>
		@endif
		@if ( session('errors') )
		<div>{{session('errors')->first('message1')}}</div>
		@endif
		<form class="form" action="/send" method="post">
		{{csrf_field( )}}
			<label>
				<span>телефон:</span>
				<input type="number" name="phone" required>
			</label>
			<label>
				<span>имя:</span>
				<input name="fio" required>
			</label>
			<label>
				<span>само обращение:</span>
				<textarea name="message" required></textarea>
			</label>
			<label>
				<span>отправить</span>
				<input type="submit" value="&rarr;">
			</label>
		</form>
    </body>
</html>